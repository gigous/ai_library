﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataMgmt;
using Probability;

namespace AADIToolbox
{
	public class CEA
	{
		Probability.Probability Prob = Probability.Probability.Instance;

		// The number of input nodes for the neural networks
		protected int numInputs;
		// The number of hidden layer nodes for the neural networks
		protected int numHidden;
		// The number of output nodes for the neural networks
		protected int numOutputs;
		// The number to scale the inputs by
		protected double[] scaling;
		// The number of generations to run the algorithm
		private int numGens;
		// The size of the population after each selection
		private int popSize;
		// The standard deviation from mean 0 to change weights
		private double stdDev;
		// The proportion of weights to alter per mutation; 1.0 means mutate all of them
		private int rad;
		// How many agents there are
		private int numPops;
		// The current population of nerual networks
		private List<List<FitNN>> populations;

		// If agents get same reward
		private bool cooperative = true;

		public bool Cooperative
		{
			get { return cooperative; }
			set { cooperative = value; }
		}

		public ICEA Domain;

		public CEA(int inputs, int hidden, int outputs, double[] scaling, int populationSize, int generations, double sd, double radiation, int numPops)
		{
			numInputs = inputs;
			numHidden = hidden;
			numOutputs = outputs;
			this.scaling = scaling;
			numGens = generations;
			popSize = populationSize;
			stdDev = sd;
			this.numPops = numPops;
			// We'll have at most 2k NNs, where k is population size of original population
			populations = new List<List<FitNN>>(numPops);

			for (int n = 0; n < numPops; n++)
			{
				populations.Add(new List<FitNN>(popSize * 2));
				// Create initial populations
				for (int i = 0; i < popSize; i++)
				{
					populations[n].Add(new FitNN(inputs, hidden, outputs, scaling));
				}
			}

			rad = (int)Math.Round(radiation * populations[0][0].NumWeights);
		}

		public List<FitNN> Search()
		{
			for (int g = 0; g < numGens; g++)
			{
				Mutate();
				Simulate();
				SelectFittest();
			}
			return FinalSelection();
		}

		private void Mutate()
		{

			// iterate through all pops
			for (int n = 0; n < numPops; n++)
			{
				// Iterate through current population
				for (int i = 0; i < popSize; i++)
				{
					// Clone NN so we add a mutated version
					FitNN nnMut = populations[n][i].Clone();

					// Get a list of random indices between 0 and (2k - 1) of size rad
					var weightsToMutate = FunWithLists.RandomList((uint)nnMut.NumWeights, (uint)rad);

					// Go through list of weights and change the corresponding weights in the NN
					foreach (int w in weightsToMutate)
					{
						nnMut.SetWeight(w, nnMut.GetWeight(w) + BoxMullerTransform.generate(0, stdDev));
					}

					// Add mutated NN to the population
					populations[n].Add(nnMut);
				}
			}
		}

		// Runs simulation in a specific domain to test fitness of several NNs
		private void Simulate()
		{
			var teams = new List<List<FitNN>>(popSize * 2);
			List<FitNN> team;
			int tempIndex;
			for (int i = 0; i < popSize * 2; i++)
			{
				team = new List<FitNN>();
				for (int n = 0; n < numPops; n++)
				{
					tempIndex = Prob.Next(populations[n].Count);
					team.Add(populations[n][tempIndex]);
					populations[n].RemoveAt(tempIndex);
				}
				teams.Add(team);
			}
			for (int t = 0; t < popSize * 2; t++)
			{
				double[] fitnesses = Domain.NNFitness(teams[t]);
				// Probably do away with this.
				double sum = 0;
				for (int n = 0; n < numPops; n++)
				{
					if (cooperative)
						sum += fitnesses[n]; // Calculate "not-really-G"
					else teams[t][n].fitness = fitnesses[n];
				}
				if (cooperative)
					for (int n = 0; n < numPops; n++)
						teams[t][n].fitness = sum;
			}
			populations = new List<List<FitNN>>();
			for (int n = 0; n < numPops; n++)
				populations.Add(new List<FitNN>());
			for (int i = 0; i < popSize * 2; i++)
			{
				for (int n = 0; n < numPops; n++)
					populations[n].Add(teams[i][n]);
			}
			teams.Clear();
		}

		// Select the fittest individuals for the current generation
		private void SelectFittest()
		{
			foreach (var population in populations)
			{
				// Create a list of all the indices of the NNs in the population
				// and randomly arrange them.
				var combatants = FunWithLists.ShuffledRange((uint)popSize * 2);

				var losers = new List<int>();

				/* I know this looks intimidating, but it's not that bad.
				 * 
				 * n is the number of NNs removed from the population thus far
				 * i is the index of the first "combatant" in the binary tournament
				 * j is the index of the second "combatant"
				 */
				for (int n = 0, i = 0, j = 1; n < popSize; n++, i += 2, j += 2)
				{
					// First combatant and second combatant are selected to compete
					int a = combatants[i];
					int b = combatants[j];
					var nnA = population[a];
					var nnB = population[b];

					// Compare fitnesses and eliminate weaker of the two.
					if (nnA.fitness > nnB.fitness) losers.Add(b);
					else if (nnA.fitness < nnB.fitness) losers.Add(a);
					else
					{
						// Or if they are equally fit, eliminate one randomly
						if (Prob.NextDouble() >= 0.5)
							losers.Add(b);
						else losers.Add(a);
					}
				}

				// sort the list of losers so they're easier to remove
				losers.Sort();

				for (int n = 0; n < popSize; n++)
				{
					// Loser elements start with lowest number (now that it's sorted)
					// So as remove losers starting from end
					population.RemoveAt(losers[popSize - n - 1]);
				}
			}
		}

		// Returns the best NN after all generations
		private List<FitNN> FinalSelection()
		{
			List<FitNN> finalTeam = new List<FitNN>(numPops);
			// perhaps not the best way to do this
			foreach (var population in populations)
			{
				FitNN final = population[0];
				foreach (var nn in population)
				{
					if (final.fitness < nn.fitness)
						final = nn;
				}
				finalTeam.Add(final);
			}
			return finalTeam;
		}
	}
}
