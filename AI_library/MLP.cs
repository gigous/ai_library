﻿using System;
using System.Collections.Generic;
using DataMgmt;

namespace AADIToolbox
{
	public class MLP : NN
	{
		// The learning rate
		protected double eta;

		public MLP(int inputs, int hidden, int outputs, double[] scaling, double eta)
			: base(inputs, hidden, outputs, scaling)
		{
			this.eta = eta;
		}

		// TODO
		/* Train the neural network using training data
		 * The NN will use the current weights
		 * INPUTS:	filename - a csv file that contains the training data
		 * 				the data must have appropriate number of columns 
		 * 				for input data, followed by columns for output
		 * 				data
		 */
		public virtual List<List<double>> Train(string inputFile)
		{
			return Train(inputFile, "");
		}

		// TODO
		// Should we output to a file or just not worry about it?
		public virtual List<List<double>> Train(string inputFile, string outputFile)
		{
			var inputCSV = new CSVHelper(inputFile);

			List<List<double>> inputData = inputCSV.Read();
			List<List<double>> outputData = new List<List<double>>(inputData.Count);

			// Shuffle the training data

			FunWithLists.Shuffle(ref inputData);

			// Number of training sample
			int n = 0;

			foreach (var row in inputData)
			{
				// Since row contains both inputs and expected outputs,
				// only pass inputs (remember GetRange makes shallow copy)
				FeedForward(row.GetRange(0, numInputs));

				var targets = row.GetRange(numInputs, numOutputs);

				// Update the weights, passing in the target values
				BackPropogate(targets);

				outputData.Add(new List<double>());

				for (int k = 0; k < numOutputs; k++)
				{
					outputData[n].Add(targets[k]);
				}
				for (int k = 0; k < numOutputs; k++)
				{
					outputData[n].Add(outputNodes[k]);
				}

				n++;
			}

			if ("" != outputFile)
			{
				var outputCSV = new CSVHelper(outputFile);
				// Write outputs to a .csv file
				outputCSV.Write(outputData);
			}
				
			return outputData;
		}

		public virtual List<List<double>> Test(string inputFile)
		{
			return Test(inputFile, "");
		}

		/* Test the neural network with test data
		 * The NN will use the current weights
		 * INPUTS:	filename - a csv file that contains the test data
		 * 				the data must have appropriate number of columns 
		 * 				for input data
		 */
		public virtual List<List<double>> Test(string inputFile, string outputFile)
		{
			//ClearMSE();

			var inputCSV = new CSVHelper(inputFile);

			// Read data into List of Lists of double and stuff
			List<List<double>> inputData = inputCSV.Read();
			List<List<double>> outputData = new List<List<double>>(inputData.Count);

			// Number of examples
			int n = 0;

			try
			{
				foreach (var row in inputData)
				{
					// Feed in inputs from data from csv file
					FeedForward(row.GetRange(0, numInputs));

					List<double> targets = row.GetRange(numInputs, numOutputs);

					outputData.Add(new List<double>());

					for (int k = 0; k < numOutputs; k++)
					{
						outputData[n].Add(targets[k]);
						//	MSE[k] += Math.Pow(targets[k] - outputNodes[k], 2);
					}
					for (int k = 0; k < numOutputs; k++)
					{
						outputData[n].Add(outputNodes[k]);
					}

					n++;
				}
			}
			catch
			{
				Console.WriteLine("Something went wrong with accessing/changing weights.");
			}

			if ("" != outputFile)
			{
				var outputCSV = new CSVHelper(outputFile);
				// Write outputs to a .csv file
				outputCSV.Write(outputData);
			}

			return outputData;
		}

		protected virtual void BackPropogate(List<double> targets)
		{
			// Holds delta values for each output node
			var deltaK = new List<double>();

			for (int k = 0; k < numOutputs; k++)
			{
				// Just a bit faster this way
				double output = outputNodes[k];
				double error = targets[k] - output;

				deltaK.Add(error * output * (1 - output));
			}

			// Holds delta values for each hidden node
			var deltaJ = new List<double>();

			for (int j = 0; j < numHidden + 1; j++)
			{
				deltaJ.Add(0);
				for (int k = 0; k < numOutputs; k++)
				{
					deltaJ[j] += getOuterWeight(j, k) * deltaK[k];
				}

				// Just a bit faster this way
				double hidden = hiddenNodes[j];
				deltaJ[j] *= hidden * (1 - hidden);
			}

			// Update inner weights
			for (int i = 0; i < numInputs + 1; i++)
			{
				for (int j = 0; j < numHidden; j++)
				{
					setInnerWeight(i, j, getInnerWeight(i, j) + eta * deltaJ[j] * inputNodes[i]);
				}
			}

			// Update outer weights
			for (int j = 0; j < numHidden + 1; j++)
			{
				for (int k = 0; k < numOutputs; k++)
				{
					setOuterWeight(j, k, getOuterWeight(j, k) + eta * deltaK[k] * hiddenNodes[j]);
				}
			}
		}
	}
}

