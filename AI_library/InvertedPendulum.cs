﻿using System;
using System.Collections.Generic;
using AADIToolbox;

using DataMgmt;

namespace Domains
{
	public class InvertedPendulum : INEAlg
	{
		Probability.Probability Prob = Probability.Probability.Instance;

		// Acceleration of gravity
		private static double g = 9.8;
	
		private static double defaultMassCart = 1.0;
		private static double defaultMassPole = 0.1;
		private static double defaultLengthPole = 0.5;
		private static double defaultMaxForce = 200.0;
		private static double defaultTrackLength = 2.0;

		// Mass of the cart
		private double M;
		// Mass of the pole
		private double m;
		// Length of the pole
		private double l;
		// Max force that can be applied to the cart
		private double Fmax;

		private double Flast = 0;
		// 1/(M+m)
		private double a;

		// Total length of track
		private double trackLength;

		// Position of the cart
		private double x;
		// Velocity of the cart
		private double xDot;
		// Angle of the pole (radians)
		private double theta;
		// Angular velocity of the pole; (radians per second)
		private double thetaDot;

		List<List<double>> vars = new List<List<double>>(100000000);

        // Tick counter for simulation
        private int t = 0;
		//u = 0;
		// Maximum number of ticks per sim
		private static int tMax = 5000;

		public List<List<double>> mydata = new List<List<double>>();

		private double F1, F2, F3, F4, F5;
		private int w1, w2, w3, w4, w5;

		public InvertedPendulum(int w1, int w2, int w3, int w4, int w5)
		{
			M = defaultMassCart;
			m = defaultMassPole;
			l = defaultLengthPole;
			Fmax = defaultMaxForce;
			trackLength = defaultTrackLength;

			this.w1 = w1;
			this.w2 = w2;
			this.w3 = w3;
			this.w4 = w4;
			this.w5 = w5;

			a = 1/(M+m);
		}

		public InvertedPendulum(double massCart, double massPole, double lengthPole, 
			double forceMax, double lengthTrack, int w1, int w2, int w3, int w4, int w5)
		{
			M = massCart;
			m = massPole;
			l = lengthPole;
			Fmax = forceMax;
			trackLength = lengthTrack;

			this.w1 = w1;
			this.w2 = w2;
			this.w3 = w3;
			this.w4 = w4;
			this.w5 = w5;

			a = 1/(M+m);
		}

		// Resets the pendulum's state for a new simulation
		public void ResetPend()
		{
			// Set position of cart to random place in between bounds of track
			x = trackLength * (Prob.NextDouble() - 0.5)/2;
			// Initialize angle of pole between pi/6 and -pi/6 
			theta = Math.PI/6.0 * 2 *(Prob.NextDouble() - 0.5);

			xDot = 0;
			thetaDot = 0;
			t = 0;
			F1 = F2 = F3 = F4 = F5 = 0;
		}

		/* Updates the state of the pendulum
		 * INPUTS:	F - 0 means full left force, 1 means full right force, 0.5 means no force
		 * 			dt - slice of time
		 */
		public void Update(double force, double dt = 0.01)
		{
			double cosTheta = Math.Cos(theta);
			double sinTheta = Math.Sin(theta);

			// Very slight damping because otherwise it will oscillate like cray with no force :( (yes, I said cray)
			thetaDot -= thetaDot*0.0001;

			// Calculate angular accel
			double temp = a*(m*l*thetaDot*thetaDot*sinTheta + force*Fmax);
			double thetaDotDot = g*sinTheta - cosTheta*temp;
			thetaDotDot /= l*(4.0/3.0 - a*m*cosTheta*cosTheta);

			// Calculate regular accel :>
			double xDotDot = temp - a*m*l*thetaDotDot*cosTheta;

			// Correct theta if needed
			while (theta >= Math.PI) theta -= 2.0 * Math.PI;
			while (theta < -Math.PI) theta += 2.0 * Math.PI;

//			mydata.Add(new List<double>());
//			mydata[mydata.Count-1].Add(theta);

			theta += thetaDot*dt;
			thetaDot += thetaDotDot*dt;

			x += xDot*dt;
			xDot += xDotDot*dt;

			Flast = force;
		}

		double INEAlg.NNFitness(FitNN nn)
		{
			if (nn.NumInputs != 4 || nn.NumOutputs != 1)
			{
				Console.WriteLine("ERROR: InvertedPendulum: Can't evaluate fitness. Must have 4 inputs and 1 output.");
				return 0;
			}

			var inputs = new List<double>(4);

			for (int i = 0; i < 4; i++) inputs.Add(0);

			ResetPend();

			double penalty = 0;

			while (!EndSimulation())
			{
				inputs[0] = x;
				inputs[1] = xDot;
				inputs[2] = theta;
				inputs[3] = thetaDot;

				nn.FeedForward(inputs);

				double F = 2 * (nn.GetOutputs()[0] - 0.5);

				if (t > 0)
				{
					if (Math.Abs(F - Flast) > 0.5)
						penalty += 1000;
				}

				// The single output is used to update the pend's state 
				Update(F, 0.01);

//				vars.Add(new List<double>(4));
//				vars[u].Add(x);
//				vars[u].Add(xDot);
//				vars[u].Add(theta);
//				vars[u].Add(thetaDot);
//				vars[u].Add(F);

				//Console.WriteLine(2*(nn.GetOutputs()[0] - 0.5));

				F1 += Math.Abs(x);
				F2 += Math.Abs(xDot);
				F3 += Math.Abs(theta);
				F4 += Math.Abs(thetaDot);
				
//				u++;
				t++;
			}
//			vars.Add(new List<double>());
//			u++;
				
			F1 /= t*trackLength/2;
			F2 /= t*1.0;
			F3 /= t*Math.PI/3.0;
			F4 /= t*3.0;
			double T = 0.02*tMax;
			double Ts = 0.02*t;
			F5 = 1 - Ts/T;

			double fitness = 1/(w1*F1 + w2*F2 + w3*F3 + w4*F4 + w5*F5 + penalty);

//			vars[u-1].Add(fitness);
			//Console.WriteLine("Survived " + t.ToString() + " time steps.");
			return fitness;
		}

		private bool EndSimulation()
		{
			if (t >= tMax)
				return true;
			else if (Math.Abs(x) > trackLength/2.0)
				return true;
			else if (Math.Abs(theta) > Math.PI/3.0)
				return true;
			else return false;
		}

		public void TestNN(FitNN nn)
		{
			var inputs = new List<double>(4);

			for (int i = 0; i < 4; i++) inputs.Add(0);

			ResetPend();

			while (!EndSimulation())
			{
				inputs[0] = x;
				inputs[1] = xDot;
				inputs[2] = theta;
				inputs[3] = thetaDot;

				nn.FeedForward(inputs);

				double F = 2 * (nn.GetOutputs()[0] - 0.5);

				Update(F, 0.01);

				vars.Add(new List<double>(4));
				vars[t].Add(x);
				vars[t].Add(xDot);
				vars[t].Add(theta);
				vars[t].Add(thetaDot);
				vars[t].Add(F);

				t++;
			}

//			WriteFile();
		}

		public void printTheta()
		{
			Console.WriteLine(theta);
		}

		public void WriteFile()
		{
			CSVHelper helper = new CSVHelper("/home/brandon/Desktop/myshit.csv");
			helper.Write(vars);
		}
	}
}
