﻿using System;

namespace Probability
{
	// see https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
	public class BoxMullerTransform
	{
		public BoxMullerTransform ()
		{
			// do nothing
		}

		public static double generate(double mean, double sd)
		{
			Random rand = new Random();
			double u1 = rand.NextDouble();
			double u2 = rand.NextDouble();
			return mean + sd * (Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2));
		}
	}
}
