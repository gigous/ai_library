﻿#include "agent.hpp"
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <math.h>

#include <vector>

#define NUM_EPS 1000
#define NUM_AGENTS 100
#define NUM_RUNS 100

#define G 		1
#define L		2
#define D		3

#define REWARD	D

using namespace std;

double f(int a)
{
	return a * exp(-a/5.0);
}

int main()
{
	srand(time(NULL));

	vector<Agent> agents;

	int occupancy[7];

	for (int ag = 0; ag < NUM_AGENTS; ag++)
		agents.push_back(Agent());

	double alpha = 0.1;

	ofstream data;
	data.open("reward.csv");

	for (int run = 0; run < NUM_RUNS; run++)
	{
		for (int ep = 0; ep < NUM_EPS; ep++)
		{
			// reset occupancy thing
			for (int i = 0; i < 7; i++)
				occupancy[i] = 0;

			for (int ag = 0; ag < NUM_AGENTS; ag++)
			{	
				// make the agent choose
				// We already chose for the first one, so skip it
				int eg = 0;
				if (0 != ep)
					eg = agents[ag].epsilon_greedy();
				if (0 == ag)
				{
					if (0 == run)
					{
						if (1 == eg)
							cout << "agent 0 chose ";
						else
							cout << "agent 0 randomly chose ";
						cout << agents[0].choice << endl;
					}
				}
				// add 1 to the occupancy for the night the agent chose
				occupancy[agents[ag].choice]++;
			}

			double reward = 0, reward_without_me[7];

			// Calculate G
			for (int i = 0; i < 7; i++)
			{
				int xi = occupancy[i];
				reward += xi*exp(-xi/5.0);
				reward_without_me[i] = (xi-1)*exp(-(xi-1)/5.0);
			}
			//if (reward > 11.036441442) cout << "why" << endl;
			data << reward;
			if ((NUM_EPS - 1) != ep)
				data << ",";

			// Learning phase
			switch (REWARD)
			{
				case 1: // G reward scheme
					for (int ag = 0; ag < NUM_AGENTS; ag++)
					{	
						agents[ag].update_value_table(reward, alpha);
					}
					break;
				case 2: // L reward scheme
					for (int ag = 0; ag < NUM_AGENTS; ag++)
					{	
						int x = occupancy[agents[ag].choice];
						double local_reward = x*exp(-x/5.0);
						agents[ag].update_value_table(local_reward, alpha);
					}
					break;
				case 3: // D reward scheme
					for (int ag = 0; ag < NUM_AGENTS; ag++)
					{	
						int xi = occupancy[agents[ag].choice];
						agents[ag].update_value_table(f(xi) - f(xi-1), alpha);
					}
					break;
			}

			// if RUN == 0
			if (run == 0)
				agents[0].print_value_table();
		}
		data << endl;

		for (int ag = 0; ag < NUM_AGENTS; ag++)
		{
			agents[ag].restart();
		}
	}

	data.close();

	for (int i = 0; i < 7; i++)
	{
		cout << occupancy[i] << " " << endl;
	}
	return 0;
}
