﻿using System;
using System.Collections.Generic;

namespace DataMgmt
{
	public class FunWithLists
	{
		static Probability.Probability Prob = Probability.Probability.Instance;

		public FunWithLists ()
		{
		}

		// Shuffles a List
		public static void Shuffle<T>(ref List<T> data)
		{
			// Get number of samples
			int n = data.Count;

			while (n > 1)
			{
				// Get random number below number of samples
				int k = Prob.Next(n--);
				// Save the randomly selected List
				var N = data[n];
				var K = data[k];

				data.RemoveAt(k);
				data.Insert(k, N);
				data.RemoveAt(n);
				data.Insert(n, K);
			}
		}

		public static List<int> ShuffledRange(uint stop)
		{
			return ShuffledRange(0, stop);
		}

		/*
		 *  Creates a range of numbers and shuffles it.
		 *  INPUTS:		start -	the number the sequence starts at
		 *  			stop - 	the number to stop at (EXCLUSIVE)
		 * 				step -	the amount to skip (optional)
		 */
		public static List<int> ShuffledRange(uint start, uint stop, uint step = 1)
		{
			var range = Range(start, stop, step);
			Shuffle(ref range);
			return range;
		}

		/*
		 *  Creates a range of numbers.
		 *  INPUTS:		start -	the number the sequence starts at
		 *  			stop - 	the number to stop at (EXCLUSIVE)
		 * 				step -	the amount to skip (optional)
		 */
		public static List<int> Range(uint start, uint stop, uint step = 1)
		{
			var list = new List<int>();
			for (uint i = start; i < stop; i += step)
			{
				list.Add((int)i);
			}
			return list;
		}

		/* Creates a list of specified length of non-repeating numbers
		 * in range min to max (excluding max number)
		 * INPUTS:		max -		the maximum possible number minus 1
		 * 				length -	the length of the list of numbers
		 * 				min -		the minimum possible number (optional)
		 */
		public static List<int> RandomList(uint max, uint length, uint min = 0)
		{
			if ((max - min) < length) return null;

			var range = Range(min, max);

			var list = new List<int>();
			for (int i = 0; i < length; i++)
			{
				int idx = Prob.Next(Int32.MaxValue) % range.Count;
				list.Add(range[idx]);
				range.RemoveAt(idx);
			}

			return list;
		}
	}
}
