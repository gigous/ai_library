﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AADIToolbox
{
    public interface ICEA
    {
        double[] NNFitness(List<FitNN> nns);
    }
}
