﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AADIToolbox;
using DataStructs;
using DataMgmt;

namespace Domains
{
	public class GridWorld : INEAlg, ICEA
	{
		Probability.Probability Prob = Probability.Probability.Instance;

		private Duo<int, int> goal;
		private int width = 0;
		private int height = 0;
		private double terminalReward;
		private double movePenalty;
		private int goalStartX;
		private int goalStartY;
		private int maxTimeStep = 500;

		// Number of times to run
		private int numTrials = 1;

		// Whether or not the goal will move in a random direction each time step
		private bool goalMoves = false;

		public bool GoalMoves
		{
			get { return goalMoves; }
			set { goalMoves = value; }
		}

		public int NumTrials
		{
			get { return numTrials; }
			set { numTrials = value; }
		}

		public int MaxTimeStep
		{
			get { return maxTimeStep; }
			set { maxTimeStep = value; }
		}

		// Create the Gridworld
		// INPUTS:  width -			how many cells in width
		//		  height -		how many cells high
		//		  goalStart -	 the cell to place the goal initially
		//		  terminalReward -how much reward landing in the goal
		//			movePenalty -	the penalty given for each timestep (<= 0)
		public GridWorld(int width, int height, int goalStartX, int goalStartY, double terminalReward, double movePenalty)
		{
			this.width = width;
			this.height = height;
			this.goalStartX = goalStartX;
			this.goalStartY = goalStartY;
			this.goal = new Duo<int, int>(goalStartX, goalStartY);
			// Goal can't be outside of world (I used an exception, but idk really how it works)
			if (goal.first >= width || goal.second >= height) throw new Exception();
			this.terminalReward = terminalReward;
			this.movePenalty = movePenalty;
		}

		// See if a single agent has reached the goal
		// INPUTS:	agentLoc - location of the agent
		// RETURNS:	true if agent on goal, else false
		private bool CheckIfReachedGoal(Duo<int, int> agentLoc)
		{
			// Check if agent is on the goal
			if (agentLoc.first == goal.first && agentLoc.second == goal.second)
				return true; // Got it!
			else return false; // Nope...
		}

		// See if any agents have reached the goal
		// INPUTS:	agentLocs - locations of the agents
		// RETURNS:	index of first agent that reached the goal
		private int CheckIfReachedGoal(List<Duo<int,int>> agentLocs)
		{
			// Iterate through each agent's location
			for (int i = 0; i < agentLocs.Count; i++)
				// Check if an agent is on the goal
				if (CheckIfReachedGoal(agentLocs[i]))
					return i; // Got it!
			// No agents have reached goal
			return -1;
		}

		// Moves a thing one cell in one direction, if possible
		// INPUTS:	entity -	the thing to move (agent, goal, something else)
		//			direction -	the direction to move the thing in; 0 is up, 1 is left, 2 is down, 3 is right
		// OUTPUTS: entity, with location changed accordingly
		private void MoveEntity(ref Duo<int,int> entity, int direction)
		{
			int x = entity.first;
			int y = entity.second;
			switch (direction)
			{
				case 0:
					entity.second = (y == 0) ? y : y - 1;
					break;
				case 1:
					entity.first = (x == 0) ? x : x - 1;
					break;
				case 2:
					entity.second = (y == height - 1) ? y : y + 1;
					break;
				case 3:
					entity.first = (x == width - 1) ? x : x + 1;
					break;
			}
		}

		// Calculates the manhattan distance from an agent to a goal for each direction
		// INPUTS:	agentLocs - list of locations of the agents
		// RETURNS:	manhattan distances to goal for each agent
		private List<Duo<int, int>> DistanceToGoal(List<Duo<int,int>> agentLocs)
		{
			var dists = new List<Duo<int, int>>(agentLocs.Count);
			foreach (var loc in agentLocs)
			{
				Duo<int, int> dist = new Duo<int, int>(goal.first - loc.first, goal.second - loc.second);
				dists.Add(dist);
			}
			return dists;
		}

		// this was done quickly and dirtily
		private Duo<int, int> DistanceToOtherAgent(List<Duo<int, int>> agentLocs, int a)
		{
			int b = a == 1 ? 0 : 1;
			Duo<int, int> dist = new Duo<int, int>(
				agentLocs[b].first - agentLocs[a].first, agentLocs[b].second - agentLocs[a].second);
			return dist;
		}

		// Initializes agents by putting them in the same random location
		// INPUTS:	numAgents - how many agent locations to spit out
		// RETURNS:	a list of the same location cuz why not
		private List<Duo<int,int>> InitAgents(int numAgents)
		{
			// Create agents one by one
			Duo<int, int> agentLoc = new Duo<int, int>();
			// initialize agent to random location
			do
			{
				agentLoc.first = Prob.Next(width);
				agentLoc.second = Prob.Next(height);
			} while (CheckIfReachedGoal(agentLoc)); // Don't want agent to initially be on goal

			// agents is a list of agent locations
			List<Duo<int, int>> agents = new List<Duo<int, int>>(numAgents);
			// Just add numAgents of the same location
			for (int n = 0; n < numAgents; n++)
				agents.Add(new Duo<int,int>(agentLoc));

			return agents;
		}

		public double[] Simulate(List<FitNN> nns, bool log = false)
		{
			goal = new Duo<int, int>(goalStartX, goalStartY);

			double[] reward = new double[nns.Count];
			for (int i = 0; i < nns.Count; i++)
				reward[i] = 0;
			// create agents and put them in one random place, together
			var agentLocs = InitAgents(nns.Count);

			var agentHist = new List<List<Duo<int, int>>>(maxTimeStep);
			var goalHist = new List<Duo<int, int>>(maxTimeStep);
			if (log)
			{
				List<Duo<int, int>> locs = new List<Duo<int, int>>(5);
				foreach (var loc in agentLocs)
					locs.Add(new Duo<int,int>(loc));
				agentHist.Add(new List<Duo<int,int>>(locs));
				goalHist.Add(new Duo<int,int>(goal));
			}

			int step = 0;
			int winner = -1;
			do // 1 iteration = 1 timestep
			{

				//// input state is distance to goal
				//var temp = new List<Duo<int, int>>(nns.Count);
				//for (int i = 0; i < nns.Count; i++)
				//{
				//	var loc = agentLocs[i];
				//	temp.Add(loc);
				//}
				var dist = DistanceToGoal(agentLocs);
				// Cause we can't alter a list element by indexing it :\
				var newAgentLocs = new List<Duo<int, int>>(nns.Count);
				for (int i = 0; i < nns.Count; i++)
				{
					var state = new List<double>(4); state.Add(dist[i].first); state.Add(dist[i].second);
					if (nns.Count > 1)
					{
						var distAgent = DistanceToOtherAgent(agentLocs, i);
						state.Add(distAgent.first);
						state.Add(distAgent.second);
					}
					nns[i].FeedForward(state);
					List<double> outputs = nns[i].GetOutputs();
					// the direction to go is determined by the max outtut
					int action = outputs.IndexOf(outputs.Max());
					// get the current agent's location
					var loc = agentLocs[i];
					// Move the agent in the direction it wants to go
					MoveEntity(ref loc, action);
					// add it to our new list
					newAgentLocs.Add(loc);
				}
				// Update agent location list
				agentLocs = new List<Duo<int, int>>(newAgentLocs);
				// Move the goal if we gotta
				if (goalMoves)
					MoveEntity(ref goal, Prob.Next(4));
				// Give penalty for each agent
				for (int i = 0; i < nns.Count; i++)
					reward[i] += movePenalty;
				step++;

				winner = CheckIfReachedGoal(agentLocs);

				if (log)
				{
					List<Duo<int, int>> locs = new List<Duo<int, int>>(5);
					foreach (var loc in agentLocs)
						locs.Add(new Duo<int, int>(loc));
					agentHist.Add(new List<Duo<int, int>>(locs));
					goalHist.Add(new Duo<int, int>(goal));
				}
			} while (winner == -1 && step < maxTimeStep);
			// I obviously didn't think about the design of this whole thing. Fuck me, right?
			if (winner != -1) // if there WAS a winner
				reward[winner] += terminalReward;

			if (log)
				Draw(agentHist, goalHist);

			return reward;
		}

		private void Draw(List<List<Duo<int,int>>> agentLocs, List<Duo<int,int>> goalLoc)
		{
			using (FileStream fs = new FileStream("grid.txt", FileMode.Create))
			{
				using (StreamWriter writer = new StreamWriter(fs))
				{
					for (int i = 0; i < agentLocs.Count; i++)
					{
						for (int y = 0; y < height; y++)
						{
							string strRow = "";
							for (int x = 0; x < width; x++)
							{
								string myChar = "#";
								if (goalLoc[i].first == x && goalLoc[i].second == y)
									myChar = "G";

								for (int a = 0; a < agentLocs[i].Count; a++)
								{
									if (agentLocs[i][a].first == x && agentLocs[i][a].second == y)
									{
										myChar = a.ToString();
										break;
									}
								}
								strRow += myChar;
							}
							writer.WriteLine(strRow);
						}
						writer.WriteLine("");
					}
				}
			}
		}

		public double NNFitness(FitNN nn)
		{
			double sum = 0;
			Duo<int, int> goalOriginal = goal;
			for (int t = 0; t < numTrials; t++)
			{
				List<FitNN> nns = new List<FitNN>(1); nns.Add(nn);
				sum += Simulate(nns)[0];
			}

			return sum / (double)numTrials;
		}

		// takes a team of policies
		public double[] NNFitness(List<FitNN> nns)
		{
			double[] sum = new double[nns.Count];
			Duo<int, int> goalOriginal = goal;
			for (int t = 0; t < numTrials; t++)
			{
				double[] reward = Simulate(nns);
				for (int n = 0; n < nns.Count; n++)
					sum[n] += reward[n];
			}
			for (int n = 0; n < nns.Count; n++)
				sum[n] /= (double)numTrials;

			return sum;
		}
	}
}
