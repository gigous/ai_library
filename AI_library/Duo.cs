﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
	public class Duo<T, U>
	{
		public T first;
		public U second;

		public Duo()
		{

		}

		public Duo(T one, U two)
		{
			first = one;
			second = two;
		}

		public Duo(Duo<T, U> other)
		{
			this.first = other.first;
			this.second = other.second;
		}
	}
}
