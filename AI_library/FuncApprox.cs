﻿using System;
using System.Collections.Generic;
using AADIToolbox;

namespace Domains
{
	public class FuncApprox : INEAlg
	{
		public FuncApprox()
		{
			// do nothing
		}

		double INEAlg.NNFitness(FitNN nn)
		{
			if (nn.NumInputs != 1 && nn.NumOutputs != 1)
			{
				Console.WriteLine("ERROR: FuncApprox: Can't evaluate fitness. Must have 4 inputs and 1 output.");
				return 0;
			}

			double x = 0, e = 0;
			for (int n = 0; n < 100; n++)
			{
				var input = new List<double>();
				input.Add(x);
				nn.FeedForward(input);
				double y = 2*(nn.GetOutputs()[0] - 0.5);
				double t = Math.Cos(x*2*Math.PI);
				e += (t - y)*(t - y);
				x += 0.01;
			}

			return 1/e;
		}
	}
}

