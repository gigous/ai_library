﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AADIToolbox;

namespace Domains
{
	public class Bar : IRLStateless
	{
		Probability.Probability Prob = Probability.Probability.Instance;

		// How many nights are available for agents to attend the bar?
		private int numNights;
		// The bar's capacity per night
		private int capacity;

		private int[] lastWeek;

		public List<int> LastWeek
		{
			get {
				var week = new List<int>(numNights);
				for (int n = 0; n < numNights; n++)
					week.Add(lastWeek[n]);
				return week; }
		}

		public Bar(int nights, int capacity)
		{
			numNights = nights;
			this.capacity = capacity;
		}

		public double[] GlobalReward(int[] joint)
		{
			lastWeek = new int[numNights];
			for (int i = 0; i < numNights; i++)
				lastWeek[i] = 0;
			for (int ag = 0; ag < joint.Length; ag++)
				lastWeek[joint[ag]]++;

			double[] perf = new double[numNights];
			for (int n = 0; n < numNights; n++)
				perf[n] += (double)lastWeek[n] * Math.Exp(-1.0 * (double)lastWeek[n] / (double)capacity);

			return perf;
			
			//double reward = joint[0] == 5 ? 7 : 3;
			//double reward0 = reward * Prob.NextDouble();
			//double reward1 = reward * Prob.NextDouble();
			//double reward2 = reward * Prob.NextDouble();
			//double reward3 = reward * Prob.NextDouble();
			//double reward4 = reward * Prob.NextDouble();
			//double reward5 = reward * Prob.NextDouble();
			//double reward6 = reward * Prob.NextDouble();
			//return new double[] { reward0, reward1, reward2, reward3, reward4, reward5, reward6 };
		}
	}
}

