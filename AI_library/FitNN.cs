using System;

namespace AADIToolbox
{
	public class FitNN : NN
	{
		public double fitness = 0;

		public double GetWeight(int w)
		{
			return weights[w];
		}

		public void SetWeight(int w, double weight)
		{
			weights[w] = weight;
		}

		public FitNN Clone()
		{
			var nn = new FitNN(numInputs, numHidden, numOutputs, scaling);

			// Copy weights
			for (int w = 0; w < numInnerWeights + numOuterWeights; w++)
			{
				nn.weights[w] = this.weights[w];
			}

			return nn;
		}

		public FitNN (int inputs, int hidden, int outputs, double[] scaling): base(inputs, hidden, outputs, scaling)
		{
		}
	}
}

