using System;
using System.Collections.Generic;
using DataMgmt;
using Probability;

namespace AADIToolbox
{
	public class NEA
	{
		Probability.Probability Prob = Probability.Probability.Instance;

		// The number of input nodes for the neural networks
		protected int numInputs;
		// The number of hidden layer nodes for the neural networks
		protected int numHidden;
		// The number of output nodes for the neural networks
		protected int numOutputs;
		// The number to scale the inputs by
		protected double[] scaling;
		// The number of generations to run the algorithm
		private int numGens;
		// The size of the population after each selection
		private int popSize;
		// The standard deviation from mean 0 to change weights
		private double stdDev;
		// The proportion of weights to alter per mutation; 1.0 means mutate all of them
		private int rad;
		// The current population of nerual networks
		private List<FitNN> population;

		public INEAlg Domain;

		public NEA (int inputs, int hidden, int outputs, double[] scaling, int populationSize, int generations, double sd, double radiation)
		{
			numInputs = inputs;
			numHidden = hidden;
			numOutputs = outputs;
			this.scaling = scaling;
			numGens = generations;
			popSize = populationSize;
			stdDev = sd;
			// We'll have at most 2k NNs, where k is population size of original population
			population = new List<FitNN>(popSize*2);

			// Create initial population
			for (int i = 0; i < popSize; i++)
			{
				population.Add(new FitNN(inputs, hidden, outputs, scaling));
			}

            rad = (int)Math.Round(radiation * population[0].NumWeights);
        }

		public FitNN Search()
		{
			for (int g = 0; g < numGens; g++)
			{
				Mutate();
				Simulate();
				SelectFittest();
			}
			return FinalSelection();
		}

		private void Mutate()
		{
			// Iterate through current population
			for (int i = 0; i < popSize; i++)
			{
				// Clone NN so we add a mutated version
				FitNN nnMut = population[i].Clone();

				// Get a list of random indices between 0 and (2k - 1) of size rad
				var weightsToMutate = FunWithLists.RandomList((uint)nnMut.NumWeights, (uint)rad);

				// Go through list of weights and change the corresponding weights in the NN
				foreach (int w in weightsToMutate)
				{
					nnMut.SetWeight(w, nnMut.GetWeight(w) + BoxMullerTransform.generate(0, stdDev));
				}

				// Add mutated NN to the population
				population.Add(nnMut);
			}
		}

		// Runs simulation in a specific domain to test fitness of each NN
		private void Simulate()
		{
			foreach (var nn in population)
				nn.fitness = Domain.NNFitness(nn);
		}
			
		// Select the fittest individuals for the current generation
		private void SelectFittest()
		{
			// Create a list of all the indices of the NNs in the population
			// and randomly arrange them.
			var combatants = FunWithLists.ShuffledRange((uint)popSize*2);

			var losers = new List<int>();

			/* I know this looks intimidating, but it's not that bad.
			 * 
			 * n is the number of NNs removed from the population thus far
			 * i is the index of the first "combatant" in the binary tournament
			 * j is the index of the second "combatant"
			 */
			for (int n = 0, i = 0, j = 1; n < popSize; n++, i += 2, j += 2)
			{
				// First combatant and second combatant are selected to compete
				int a = combatants[i];
				int b = combatants[j];
				var nnA = population[a];
				var nnB = population[b];

				// Compare fitnesses and eliminate weaker of the two.
				if 		(nnA.fitness > nnB.fitness) losers.Add(b);
				else if (nnA.fitness < nnB.fitness) losers.Add(a);
				else
				{
					// Or if they are equally fit, eliminate one randomly
					if (Prob.NextDouble() >= 0.5)
						losers.Add(b);
					else losers.Add(a);
				}
			}

			// sort the list of losers so they're easier to remove
			losers.Sort();

			for (int n = 0; n < popSize; n++)
			{
				// Loser elements start with lowest number (now that it's sorted)
				// So as remove losers starting from end
				population.RemoveAt(losers[popSize - n - 1]);
			}
		}

		// Returns the best NN after all generations
		private FitNN FinalSelection()
		{
			FitNN final = population[0];
			foreach (var nn in population)
			{
				if (final.fitness < nn.fitness)
					final = nn;
			}
			return final;
		}
	}
}

