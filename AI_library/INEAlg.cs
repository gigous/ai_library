﻿using System;
using System.Collections.Generic;

namespace AADIToolbox
{
	public interface INEAlg
	{
		double NNFitness(FitNN nn);
	}
}

