﻿using System;
using System.IO;
using System.Collections.Generic;

// TODO: Extend to support multiple types
// TODO: Store file contents and allow manipulation?
// TODO: Handle ";" as newline in CSV file?]
// TODO: support massive number of ouput/input files?

namespace DataMgmt
{
	public class CSVHelper
	{
		// The name of the file to read or write
		private string filename;

		// The number of rows in the file
		private int rows = 0;

		// The number of columns in the file
		private int cols = 0;

		// Keep track of the type of T
		//private Type dataType = typeof(T);

		// Constructor
		public CSVHelper()
		{
			// nothing
		}

		// Constructor, specifies filename
		public CSVHelper(string filename)
		{
			this.filename = filename;
		}

		// Read from the CSV file and store into a List of Lists
		// Indices: [row][column]
		public List<List<double>> Read()
		{
			try
			{
				// Get a new StreamReader object to read the file
				var reader = new StreamReader(File.OpenRead(filename));

				// The contents of the file are a List of rows with a List of elements (columns)
				var contents = new List<List<double>>();

				// Go through all the rows
				while (!reader.EndOfStream)
				{
					rows++;

					string[] values = reader.ReadLine().Split(',');

					var row = new List<double>();

					foreach (string value in values)
					{
						row.Add(Convert.ToDouble(value));
					}

					contents.Add(row);
				}

				// Let's wrongly assume that the number of columns is equal to the first row's
				// number of elements :)
				// TODO: find a better way
				cols = contents[0].Count;

				return contents;
			}
			catch (FileNotFoundException)
			{
				Console.WriteLine("CSVHelper ERROR: File not found: " + filename);
				return new List<List<double>>();
			}
		}

		public void Write(List<List<double>> contents)
		{
			using (FileStream fs = new FileStream(filename, FileMode.Create))
			{
				using (StreamWriter writer = new StreamWriter(fs))
				{
					foreach (var row in contents)
					{
						string strRow = "";
						for (int i = 0; i < row.Count; i++)
						{
							strRow += row[i].ToString();
							if (i == row.Count - 1) break;
							strRow += ",";
						}
						writer.WriteLine(strRow);
					}
				}
			}
		}

		// for a list of integers
		public void Write(List<int> contents)
		{
			using (FileStream fs = new FileStream(filename, FileMode.Create))
			{
				using (StreamWriter writer = new StreamWriter(fs))
				{
					string strRow = "";
					for (int i = 0; i < contents.Count; i++)
					{
						strRow += contents[i].ToString();
						if (i == contents.Count - 1) break;
							strRow += ",";
					}
					writer.WriteLine(strRow);
				}
			}
		}

		public void WriteStatisticsBar(List<List<double>> contents, int errorBarSkip = 1)
		{
			int numRows = contents.Count;
			int numCols = contents[0].Count;

			List<double> avg = new List<double>(numCols);
			List<double> err = new List<double>(numCols);

			for (int i = 0; i < numCols; i++)
			{
				avg.Add(0.0);
				err.Add(0.0);
			}

			foreach (var row in contents)
			{
				for (int i = 0; i < numCols; i++)
				{
					avg[i] += row[i];
				}
			}

			for (int i = 0; i < numCols; i++)
			{
				avg[i] /= numRows;
			}

			for (int i = 0; i < numCols; i++)
			{
				double devSqrd = 0;
				foreach (var row in contents)
				{
					devSqrd += Math.Pow(row[i] - avg[i], 2);
				}

				double sd = devSqrd / (double)numRows;

				err[i] = Math.Sqrt(sd / Math.Sqrt(numRows));
			}

			using (FileStream fs = new FileStream(filename, FileMode.Create))
			{
				using (StreamWriter writer = new StreamWriter(fs))
				{
					for (int i = 0; i < numCols; i++)
					{
						if (i % errorBarSkip == 0)
							writer.WriteLine(i + "," + avg[i].ToString() + "," + err[i].ToString());
						else
							writer.WriteLine(i + "," + avg[i].ToString());
					}
				}
			}
		}

		// Run 1 is the first row, Run 2 is the second row
		public void WriteStatistics(List<List<double>> contents)
		{
			int numRows = contents.Count;
			int numCols = contents[0].Count;

			List<double> avg = new List<double>(numCols);
			List<double> err = new List<double>(numCols);

			for (int i = 0; i < numCols; i++)
			{
				avg.Add(0.0);
				err.Add(0.0);
			}

			foreach (var row in contents)
			{
				for (int i = 0; i < numCols; i++)
				{
					avg[i] += row[i];
				}
			}

			for (int i = 0; i < numCols; i++)
			{
				avg[i] /= numRows;
			}

			for (int i = 0; i < numCols; i++)
			{
				double devSqrd = 0;
				foreach (var row in contents)
				{
					devSqrd += Math.Pow(row[i] - avg[i], 2);
				}

				double sd = devSqrd / (double)numRows;

				err[i] = Math.Sqrt(sd / Math.Sqrt(numRows));
			}


		}
	}
}
