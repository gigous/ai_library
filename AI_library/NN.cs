using System;
using System.Collections.Generic;
using DataMgmt;

// TODO: Single-layer NN
// TODO: Support for more than one hidden layer?
// TODO: Support for varying eta?

namespace AADIToolbox
{
	public class NN
	{
		Probability.Probability Prob = Probability.Probability.Instance;

		// The number of input nodes for the neural network
		protected int numInputs;
		// The number of hidden layer nodes for the neural network
		protected int numHidden;
		// The number of output nodes for the neural network
		protected int numOutputs;
		// The number to scale the inputs by
		protected double[] scaling;

		protected int numInnerWeights;
		protected int numOuterWeights;

		// The values output from the nodes
		protected double[] inputNodes;
		protected double[] hiddenNodes;
		protected double[] outputNodes;
		// The weights of the synapses from the input layer to the hidden layer
		protected double[] weights;
		// The weights of the synapses from the hidden layer to the output layer

		public int NumInnerWeights
		{
			get { return numInnerWeights; }
		}
		public int NumOuterWeights
		{
			get { return numOuterWeights; }
		}
		public int NumInputs
		{
			get { return numInputs; }
		}
		public int NumOutputs
		{
			get { return numOutputs; }
		}
		public int NumHidden
		{
			get { return numHidden; }
		}
		public int NumWeights
		{
			get { return numInnerWeights + numOuterWeights; }
		}

		/* Constructor which takes information about the NNs structure
		 * INPUTS:	inputs - number of input nodes
		 * 			hidden - number of hidden layer nodes (0 for no hidden layer)
		 * 			outputs- number of output nodes
		 * 			scaling- the number to scale the inputs by
		 */
		public NN(int inputs, int hidden, int outputs, double[] scaling)
		{
			numInputs = inputs;
			numHidden = hidden;
			numOutputs = outputs;
			this.scaling = scaling;

			inputNodes = new double[numInputs + 1];
			hiddenNodes = new double[numHidden + 1];
			outputNodes = new double[numOutputs];

			numInnerWeights = (numInputs + 1)*numHidden;
			numOuterWeights = (numHidden + 1)*numOutputs;

			weights = new double[numInnerWeights + numOuterWeights];

			Reset();

			// Create the nodes
			for (int i = 0; i < numInputs; i++)
			{
				inputNodes[i] = 0;
			}

			// One for bias. This one won't change value.
			inputNodes[numInputs] = 1;

			for (int j = 0; j < numHidden; j++)
			{
				hiddenNodes[j] = 0;
			}

			// One for bias. This one won't change value.
			hiddenNodes[numHidden] = 1;

			for (int k = 0; k < numOutputs; k++)
			{
				outputNodes[k] = 0;
				//MSE.Add(0);
			}
		}

		protected double getInnerWeight(int inputNode, int hiddenNode)
		{
			// The weight lists are arranged such that the weights from all input nodes
			// (incl. bias) to the first hidden node are listed first, and so on...
			//return innerWeights[inputNode + hiddenNode*(numHidden + 1)];
			return weights[inputNode + hiddenNode*(numInputs + 1)];
		}

		protected void setInnerWeight(int inputNode, int hiddenNode, double newWeight)
		{
			// The weight lists are arranged such that the weights from all input nodes
			// (incl. bias) to the first hidden node are listed first, and so on...
			weights[inputNode + hiddenNode*(numInputs + 1)] = newWeight;
		}

		protected double getOuterWeight(int hiddenNode, int outputNode)
		{
			// The weight lists are arranged such that the weights from all hidden nodes
			// (incl. bias) to the first output node are listed first, and so on...
			return weights[numInnerWeights + hiddenNode + outputNode*(numHidden + 1)];
		}

		protected void setOuterWeight(int hiddenNode, int outputNode, double newWeight)
		{
			// The weight lists are arranged such that the weights from all hidden nodes
			// (incl. bias) to the first output node are listed first, and so on...
			weights[numInnerWeights + hiddenNode + outputNode*(numHidden + 1)] = newWeight;
		}

		protected void RandomizeWeight(int weight)
		{
			weights[weight] = Prob.NextDouble();
		}

		public virtual void Reset()
		{
			// Initialize the weights to random values between 0 and 1
			for (int w = 0; w < numInnerWeights + numOuterWeights; w++)
			{
				RandomizeWeight(w);
			}
		}

		public List<double> GetOutputs()
		{
			var outputs = new List<double>();
			for (int i = 0; i < numOutputs; i++)
				outputs.Add(outputNodes[i]);
			return outputs;
		}
			
		/* Takes a list of inputs and feeds them through the NN
		 * INPUTS: 			the list of inputs
		 * RETURNS:			nothing
		 * SIDE EFFECTS:	the output of the NN is updated
		 */
		public virtual void FeedForward(List<double> inputs)
		{
			// Load input nodes with input data
			for (int i = 0; i < numInputs; i++)
			{
				inputNodes[i] = scaling[i] * inputs[i];
			}

			// Iterate through hidden nodes
			for (int j = 0; j < numHidden; j++)
			{
				// Keep track of sum for activation function
				double sum = 0;

				// Multiply input values (incl. bias) with the corresponding weights
				for (int i = 0; i < numInputs + 1; i++)
				{
					// Add to sum
					sum += inputNodes[i]*getInnerWeight(i, j);
				}

				// Update output of jth hidden node
				hiddenNodes[j] = Sigmoid(sum);
			}

			// Iterate through output nodes
			for (int k = 0; k < numOutputs; k++)
			{
				// Keep track of sum for activation function
				double sum = 0;

				// Multiply hidden nodes (incl. bias) with the corresponding weights
				for (int j = 0; j < numHidden + 1; j++)
				{
					// Add to sum
					sum += hiddenNodes[j]*getOuterWeight(j, k);
				}

				// Update output of jth output node
				outputNodes[k] = Sigmoid(sum);
			}
		}

		// TODO
		private double Sigmoid(double x)
		{
			return 1.0/(1.0 + Math.Exp(-x));
		}

//		public void printNodes()
//		{
//			foreach (var node in inputNodes)
//			{
//				Console.Write(node + " ");
//			}
//			Console.WriteLine();
//			foreach (var node in hiddenNodes)
//			{
//				Console.Write(node + " ");
//			}
//			Console.WriteLine();
//			foreach (var node in outputNodes)
//			{
//				Console.Write(node + " ");
//			}
//			Console.WriteLine();
//		}
	}
}

