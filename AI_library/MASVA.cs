﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AADIToolbox
{
	// Multiagent system of Value-Action agents
	public class MASVA
	{
		Probability.Probability Prob = Probability.Probability.Instance;
		private RewardType.Reward rewardStruct;

		private List<VA> agents;
		public IRLStateless domain;

		private int numAgents;
		private int numActions;
		private int numEpochs;

		public VA.ActionPolicy Policy
		{
			get { return agents[0].Policy; }
			set { foreach (var agent in agents) agent.Policy = value; }
		}
		public double Epsilon
		{
			get { return agents[0].Epsilon; }
			set { foreach (var agent in agents) agent.Epsilon = value; }
		}
		public RewardType.Reward RewardStructure
		{
			get { return rewardStruct; }
			set { rewardStruct = value; }
		}

		public MASVA(int numAgents, int numEpochs, int numActions, double alpha, double initValue = 0.0)
		{
			agents = new List<VA>();
			this.numAgents = numAgents;
			for (int ag = 0; ag < numAgents; ag++)
				agents.Add(new VA(numActions, alpha, initValue));
			rewardStruct = RewardType.Reward.Global;
			this.numEpochs = numEpochs;
			this.numActions = numActions;
		}

		public List<double> Simulate()
		{
			List<double> output = new List<double>(numEpochs);
			for (int e = 0; e < numEpochs; e++)
			{
				int[] joint = new int[numAgents];
				for (int ag = 0; ag < numAgents; ag++)
				{
					agents[ag].ChooseAction();
					joint[ag] = agents[ag].Action;
				}

				double[] perf = domain.GlobalReward(joint);
				// Calculate global performance from individual (if applicable [ex: bar]) performances
				double G = perf[0];
				for (int i = 1; i < perf.Length; i++)
					G += perf[i];
				// Add this epoch's G to the output
				output.Add(G);

				// Update rewards of agents and stuff
				switch (rewardStruct)
				{
					case (RewardType.Reward.Global):
						foreach (var agent in agents)
						{
							agent.reward = G;
							agent.UpdateTable();
						}
						break;
					// This isn't very flexible with the counterfactual thing.
					// Can probably do some voodoo to get the counterfactual calculation to be done in domain
					case (RewardType.Reward.Difference):
						for (int ag = 0; ag < numAgents; ag++)
						{
							// Make a list of all agents without the agent in question
							// (or maybe with the agent selecting a different action)
							List<VA> agentsMinusMe = new List<VA>(agents);
							agentsMinusMe.RemoveAt(ag);
							// Joint actions minus my own action
							int[] jointMinusMine = new int[numAgents];
							for (int ga = 0; ga < numAgents - 1; ga++)
							{
								//if (ga == ag)
								//	jointMinusMine[ga] = (joint[ga] + 1) % 7; // TEMP MTEPEPSI MPTMEP
								//else
								//	jointMinusMine[ga] = joint[ga];
								if (ga < ag)
									jointMinusMine[ga] = joint[ga];
								else if (ga >= ag)
									jointMinusMine[ga] = joint[ga + 1];
							}
							// Close your eyes and imagine a domain where agent ag doesn't exist... 
							perf = domain.GlobalReward(jointMinusMine);
							// Now open your eyes. Close your eyes again. What do you see?
							double GMinusMe = perf[0];
							// Nothing, dumbshit, your eyes are closed.
							for (int i = 1; i < perf.Length; i++)
								GMinusMe += perf[i];

							// Give agent the difference reward
							agents[ag].reward = G - GMinusMe;
							agents[ag].UpdateTable();
						}
						break;
					case (RewardType.Reward.Difference2):
						for (int ag = 0; ag < numAgents; ag++)
						{
							// Make a list of all agents without the agent in question
							// (or maybe with the agent selecting a different action)
							List<VA> agentsMinusMe = new List<VA>(agents);
							//agentsMinusMe.RemoveAt(ag);
							// Joint actions minus my own action
							int[] jointMinusMine = new int[numAgents];
							for (int ga = 0; ga < numAgents - 1; ga++)
							{
								if (ga == ag)
									jointMinusMine[ga] = Prob.Next(numActions); // TEMP MTEPEPSI MPTMEP
								else
									jointMinusMine[ga] = joint[ga];
								//if (ga < ag)
								//	jointMinusMine[ga] = joint[ga];
								//else if (ga >= ag)
								//	jointMinusMine[ga] = joint[ga + 1];
							}
							// Close your eyes and imagine a domain where agent ag doesn't exist... 
							perf = domain.GlobalReward(jointMinusMine);
							// Now open your eyes. Close your eyes again. What do you see?
							double GMinusMe = perf[0];
							// Nothing, dumbshit, your eyes are closed.
							for (int i = 1; i < perf.Length; i++)
								GMinusMe += perf[i];

							// Give agent the difference reward
							agents[ag].reward = G - GMinusMe;
							agents[ag].UpdateTable();
						}
						break;
					case (RewardType.Reward.Local):
						foreach (var agent in agents)
						{
							// might be dumb to assume that num of performance things is equal to num actions
							agent.reward = perf[agent.Action];
							agent.UpdateTable();
						}
						break;
				}
			}
			return output;
		}
	}
}
