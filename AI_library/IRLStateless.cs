﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AADIToolbox
{
	public interface IRLStateless
	{
		// joint = joint actions ;)
		double[] GlobalReward(int[] joint);
	}
}
