﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Probability;

namespace AADIToolbox
{
	public class VA
	{
		public enum ActionPolicy { Greedy, EpsGreedy, Softmax };

		private Probability.Probability Prob = Probability.Probability.Instance;

		private int numActions;
		private double alpha;
		private double[] V;
		// Index of the current best action
		private int maxA = 0;

		// Probability that agent chooses a random action
		private double epsilon = 0.1;

		// Which action the agent will take. Private, but accessible externally.
		private int action;
		// Reward received as a result of agent's action. Modified externally.
		public double reward;

		private ActionPolicy policy = ActionPolicy.EpsGreedy;

		public ActionPolicy Policy
		{
			get { return policy; }
			set { policy = value; }
		}
		public int Action
		{
			get { return action; }
		}
		public double Epsilon
		{
			get { return epsilon; }
			set { epsilon = value; }
		}

		public VA(int numActions, double alpha, double initValue = 0)
		{
			this.numActions = numActions;
			this.alpha = alpha;
			V = new double[numActions];
			for (int a = 0; a < numActions; a++) V[a] = initValue;
		}

		public void ChooseAction()
		{
			switch (policy)
			{
				case ActionPolicy.Greedy:

					break;
				case ActionPolicy.EpsGreedy:
					var r = Prob.NextDouble();
					if (r < epsilon)
						action = Prob.Next(numActions);
					else
					{
						action = maxA;
						// contains actions that are as good as maxA
						//List<int> eq = new List<int>(numActions);
						//eq.Add(maxA);
						//for (int a = maxA + 1; a < numActions; a++)
						//{
						//	if (V[a] == V[maxA])
						//		eq.Add(a);
						//}
						//if (eq.Count > 1)
						//	action = eq[Prob.Next(numActions) % eq.Count];
					}
					break;
				case ActionPolicy.Softmax:

					break;
			}
		}

		public void UpdateTable()
		{
			// update value table
			V[action] = V[action] + alpha * (reward - V[action]);
			// update maxA
			for (int a = 0; a < numActions; a++)
			{
				if (V[maxA] < V[a])
					maxA = a;
			}
		}

		
	}
}
