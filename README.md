# Neural Network & Single-Agent/Multi-Agent Domain Library

A collection of code written in C# for simulating and controlling agents in single- or multi-agent domains. Written by Brandon Gigous in 2015 & 2016.
